const express = require("express");
const app = express();
const cors = require("cors");
const { sequelize } = require("./models");
const errorHandler = require("./src/errorHandling");
const bulkDataBase = require("./src/bulkDataBase");
const characterDB = require("./src/db/character");
const port = 3001;

(async () => {
    try {
        await bulkDataBase(sequelize);
    } catch (error) {
        console.error(error);
    }
    const Op = sequelize.Sequelize.Op;
    character = characterDB(sequelize, Op);

    app.use(cors());

    app.get("/characters", async (req, res, next) => {
        const page = req.query.page;
        const limit = req.query.page_size || 10;
        const offset = page * limit;
        const query = req.query.query || "";

        let response;
        try {
            response = await character.getAllCharacters(offset, limit, query);
        } catch (error) {
            next(new Error("Server error"));
        }
        res.status(200).send(JSON.stringify(response));
    });

    app.get("/characters/:id", async (req, res, next) => {
        const id = req.params.id;
        let response;
        try {
            response = await character.getCharacterById(id);
        } catch (error) {
            next(new Error("Server error"));
        }

        if (!response) {
            res.status(404);
            next(new Error("Not found"));
        } else {
            res.status(200).send(response);
        }
    });

    app.use(errorHandler);

    app.listen(port, () => {
        console.log(`listening at http://localhost:${port}`);
    });
})();
