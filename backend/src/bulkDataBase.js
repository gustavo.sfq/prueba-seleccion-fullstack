const axios = require('axios');
const bulkDataBase = (sequelize) => {
    return new Promise(async (res, rej) => {
        let characters;
        try {
            console.log("loading characters...");
            characters = await axios.get(
                "https://api.got.show/api/general/characters"
            );
            characters = characters.data.book;

            console.log("delete database");
            await sequelize.models.character.destroy({
                where: {},
                truncate: true,
            });

            console.log("inserting characters...");
            characters.forEach((character) => {
                sequelize.models.character.create({
                    name: character.name ? character.name.toLowerCase() : null,
                    house: character.house
                        ? character.house.toLowerCase()
                        : null,
                    age: character.age ? character.age.age : null,
                    slug: character.slug || null,
                    rank: character.pagerank ? character.pagerank.rank : 0,
                    titles: character.titles.join(", "),
                    books: character.books.join(", "),
                    alive: character.alive || null,
                    gender: character.gender || null,
                    image: character.image || null,
                });
            });
            res();
        } catch (error) {
            console.log(error);
            rej(error);
        }
    });
};

module.exports = bulkDataBase;
