const characterDB = (db, Op) => {
    const setLike = (name, query) => {
        const like = {}
        like[name] = {
            [Op.like]: `%${query.toLowerCase()}%`,
        };
        return like
    }
    
    const getAllCharacters = async (offset, limit, query) => {
        return new Promise(async (res, rej) => {
            try {
                res(await db.models.character.findAll({
                    offset,
                    limit,
                    where: query && {
                        [Op.or]: [
                            setLike('house', query),
                            setLike('name', query),
                        ],
                    },
                }));
                
            } catch (error) {
                rej(error)
            }
        });
    }
    
    const getCharacterById = async (id) => {
        return new Promise(async (res, rej) => {
            try {
                res(await db.models.character.findOne({
                    where: {
                        id,
                    },
                }));
            } catch (error) {
                rej(error);
            }
        })
    }
    return {getAllCharacters, getCharacterById}
} 

module.exports = characterDB;