const errorHandler = (err, req, res, next) => {
    if (res.statusCode == 404) {
        res.send(JSON.stringify({message: err.message}));
    } else {
        res.status(500).send(JSON.stringify({message: err.message}));
    }
};

module.exports = errorHandler;
