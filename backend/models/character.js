'use strict';
module.exports = (sequelize, DataTypes) => {
  const character = sequelize.define('character', {
    name: DataTypes.STRING,
    house: DataTypes.STRING,
    age: DataTypes.INTEGER,
    slug: DataTypes.STRING,
    rank: DataTypes.INTEGER,
    titles: DataTypes.STRING,
    alive: DataTypes.BOOLEAN,
    gender: DataTypes.STRING,
    image: DataTypes.STRING,
    books: DataTypes.STRING
  }, {});
  character.associate = function(models) {
    // associations can be defined here
  };
  return character;
};