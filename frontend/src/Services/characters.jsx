import axios from "axios";
import Character from "../Classes/character";
const loadCharacters = async (page, query) => {
    let characters;
    try {
        const response = await axios.get(`http://localhost:3001/characters?page=${page}&query=${query}`);
        characters = response.data.map(character => new Character(character));
    } catch (error) {
        characters = [];
    }
    console.log(characters);
    return characters;
};

const loadCharactersById = async (id) => {
    let character;
    try {
        const response = await axios.get(`http://localhost:3001/characters/${id}`);
        character = new Character(response.data);
    } catch (error) {
        character = {};
    }
    return character;
};

export { loadCharacters, loadCharactersById };
