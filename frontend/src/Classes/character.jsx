import CharacterDetail from "../Components/Character/CharacterDetail";

class Character{
    constructor(attributes){
        this.id = attributes['id']
        this.name = attributes['name'] || "";
        this.house = attributes['house'] || "";
        this.slug = attributes['slug'] || "";
        this.rank = attributes['rank'] || 0;
        this.titles = attributes['titles'] || "";
        this.books = attributes['books'] || "";
        this.alive = attributes['alive'];
        this.gender = attributes['gender']|| "";
        this.image = attributes['image'] || null;
    }
}

export default Character;