import React from "react";
import { useRouteMatch } from "react-router-dom";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import CharacterDetail from "./CharacterDetail";
import CharacterList from "./CharacterList";

const Character = () => {
    const match = useRouteMatch();
    return (
        <Switch>
            <Route path={`${match.path}/detail/:id`}>
                <CharacterDetail />
            </Route>
            <Route path={`${match.path}`}>
                <CharacterList />
            </Route>
        </Switch>
    );
};

export default Character;
