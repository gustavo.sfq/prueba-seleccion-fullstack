import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { loadCharacters } from "../../../Services/characters";

import noImage from "./../../../img/no-image.png";
import "./style.css";

const CharacterList = () => {
    const [characters, setCharacters] = useState([]);
    const [page, setPage] = useState(0);
    useEffect(() => {
        (async () => {
            try {
                const response = await loadCharacters(page, searchQuery);
                if (page == 0) {
                    setCharacters(response);
                } else {
                    setCharacters([...characters, ...response]);
                }
            } catch (error) {
                console.error(error);
            }
        })();
    }, [page]);

    const [searchQuery, setSearchQuery] = useState("");
    useEffect(() => {
        (async () => {
            setPage(0);
            const response = await loadCharacters(page, searchQuery);
            setCharacters(response);
        })();
    }, [searchQuery]);

    const characterWidget = (character) => (
        <Link
            key={character.id}
            to={`characters/detail/${character.id}`}
            className="character-container"
        >
            <h2>{character.name}</h2>
            <h3>{character.house}</h3>
            <img src={character.image || noImage} alt={character.name} />
        </Link>
    );

    const searchInputChange = async (event) => {
        setSearchQuery(event.target.value);
    };

    return (
        <div className="container">
            <span>Search: </span>
            <input type="search" onChange={(e) => searchInputChange(e)} />
            <div className="character-list">
                {characters.length ? (
                    characters.map((character) => characterWidget(character))
                ) : (
                    <div>Loading</div>
                )}
            </div>
            <button onClick={() => setPage(page + 1)}>Load More</button>
        </div>
    );
};

export default CharacterList;
