import React, { useEffect, useState } from "react";
import { loadCharactersById } from "../../../Services/characters";
import { useParams } from "react-router-dom";

import noImage from "./../../../img/no-image.png";

const CharacterDetail = () => {
    const { id } = useParams();
    const [character, setCharacter] = useState(null);
    useEffect(() => {
        (async () => {
            try {
                const response = await loadCharactersById(id);
                setCharacter(response);
            } catch (error) {
                setCharacter({});
            }
        })();
    }, []);
    return (
        <div className="container">
            {character ? (
                character.name ? (
                    <div className="character-container">
                        <h2>Name: {character.name}</h2>
                        <h3>House: {character.house}</h3>
                        <h3>Gender: {character.gender}</h3>
                        <h3>Rank: {character.rank}</h3>
                        <img
                            src={character.image || noImage}
                            alt={character.name}
                        />
                        <h3>Slug: {character.slug}</h3>
                        <h3>Titles: {character.title}</h3>
                        <h3>Books: {character.books}</h3>
                    </div>
                ) : (
                    <div>Not Found</div>
                )
            ) : (
                <div>Loading...</div>
            )}
        </div>
    );
};

export default CharacterDetail;
